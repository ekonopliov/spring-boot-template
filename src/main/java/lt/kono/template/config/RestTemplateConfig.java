package lt.kono.template.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {

    String JIRA_ADMIN_USERNAME = "hack";
    String JIRA_ADMIN_PASSWORD = "Danske.1111";

    String CONFLUENCE_ADMIN_USERNAME = "hack";
    String CONFLUENCE_ADMIN_PASSWORD = "Danske.1111";

    @Bean
    @Qualifier("JiraAccessTemplate")
    public RestTemplate jiraRestTemplate(RestTemplateBuilder restTemplateBuilder){

        return restTemplateBuilder
                .basicAuthentication(JIRA_ADMIN_USERNAME, JIRA_ADMIN_PASSWORD)
                .build();
    }

    @Bean
    @Qualifier("ConfluenceAccessTemplate")
    public RestTemplate confluenceRestTemplate(RestTemplateBuilder restTemplateBuilder){

        return restTemplateBuilder
                .basicAuthentication(CONFLUENCE_ADMIN_USERNAME, CONFLUENCE_ADMIN_PASSWORD)
                .build();
    }
}
