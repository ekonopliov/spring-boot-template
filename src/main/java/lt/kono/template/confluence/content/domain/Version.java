
package lt.kono.template.confluence.content.domain;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "by",
    "when",
    "message",
    "number",
    "minorEdit",
    "hidden",
    "_links",
    "_expandable"
})
public class Version {

    @JsonProperty("by")
    private By by;
    @JsonProperty("when")
    private String when;
    @JsonProperty("message")
    private String message;
    @JsonProperty("number")
    private Integer number;
    @JsonProperty("minorEdit")
    private Boolean minorEdit;
    @JsonProperty("hidden")
    private Boolean hidden;
    @JsonProperty("_links")
    private Links____ links;
    @JsonProperty("_expandable")
    private Expandable____ expandable;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("by")
    public By getBy() {
        return by;
    }

    @JsonProperty("by")
    public void setBy(By by) {
        this.by = by;
    }

    @JsonProperty("when")
    public String getWhen() {
        return when;
    }

    @JsonProperty("when")
    public void setWhen(String when) {
        this.when = when;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("number")
    public Integer getNumber() {
        return number;
    }

    @JsonProperty("number")
    public void setNumber(Integer number) {
        this.number = number;
    }

    @JsonProperty("minorEdit")
    public Boolean getMinorEdit() {
        return minorEdit;
    }

    @JsonProperty("minorEdit")
    public void setMinorEdit(Boolean minorEdit) {
        this.minorEdit = minorEdit;
    }

    @JsonProperty("hidden")
    public Boolean getHidden() {
        return hidden;
    }

    @JsonProperty("hidden")
    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    @JsonProperty("_links")
    public Links____ getLinks() {
        return links;
    }

    @JsonProperty("_links")
    public void setLinks(Links____ links) {
        this.links = links;
    }

    @JsonProperty("_expandable")
    public Expandable____ getExpandable() {
        return expandable;
    }

    @JsonProperty("_expandable")
    public void setExpandable(Expandable____ expandable) {
        this.expandable = expandable;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
