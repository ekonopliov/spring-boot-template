package lt.kono.template.controllers;

import lombok.AllArgsConstructor;
import lt.kono.template.entities.User;
import lt.kono.template.rating.domain.ReviewRequest;
import lt.kono.template.services.ConfluenceSearchService;
import lt.kono.template.services.JiraSearchService;
import lt.kono.template.services.RatingService;
import lt.kono.template.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class MainController {

    @Autowired
    UserService userService;

    @Autowired
    RatingService ratingService;

    @Autowired
    JiraSearchService jiraSearchService;

    @Autowired
    ConfluenceSearchService confluenceSearchService;

    @GetMapping("/getLeaderboard")
    public List<User> getLeaderboard(){
        return ratingService.getAllTimeLeaderboard();
    }

    @GetMapping("/getExperts")
    public List<User> getSuggestedUsers(@RequestParam(name = "keyword", required = false) String keyword){

        return userService.getKeywordTopUsers(keyword).stream()
                .map(user -> jiraSearchService.getKeywordUserStats(user, keyword))
                .map(user -> confluenceSearchService.getKeywordUserStats(user, keyword))
                .collect(Collectors.toList());
    }

    @GetMapping("/getExpert/{username}")
    public User getUser(@PathVariable(name = "username", required = true) String username){
        return userService.getUser(username);
    }

    @PostMapping("/addUserReview")
    public ResponseEntity addUserReview(@RequestBody ReviewRequest reviewRequest){
        ratingService.addUserReview(reviewRequest);
        return ResponseEntity.ok("Success");
    }
}
