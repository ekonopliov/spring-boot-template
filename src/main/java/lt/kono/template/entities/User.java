package lt.kono.template.entities;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lt.kono.template.domain.ConfluenceArticleInfo;
import lt.kono.template.domain.JiraIssueInfo;
import lt.kono.template.jira.search.domain.Issue;

import java.util.HashMap;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

    String name;

    String key;

    String emailAddress;

    HashMap<String, String> avatarUrls;

    String displayName;

    Boolean active;

    Boolean deleted;

    String timeZone;

    String locale;

    Integer rating;

    String avatar;

    Integer solvedCases;

    Integer receivedCases;

    List<JiraIssueInfo> jiraIssues;

    List<ConfluenceArticleInfo> confluenceArticles;

    String teamName;
}
