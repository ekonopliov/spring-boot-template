package lt.kono.template.rating.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RatingSolvedResponse {

    String username;

    Integer receivedCases;

    Integer solvedCases;
}
