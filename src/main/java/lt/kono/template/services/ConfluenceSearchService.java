package lt.kono.template.services;

import lombok.SneakyThrows;
import lt.kono.template.confluence.content.domain.ConfluenceContentInfoResponse;
import lt.kono.template.confluence.search.domain.ConfluenceSearchResponse;
import lt.kono.template.confluence.search.domain.Result;
import lt.kono.template.domain.ConfluenceArticleInfo;
import lt.kono.template.entities.User;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriUtils;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ConfluenceSearchService {

    @Autowired
    @Qualifier("ConfluenceAccessTemplate")
    RestTemplate restTemplate;

    String CONFLUENCE_BASE_URL = "http://hack.kono.lt";
    String CONFLUENCE_API_SEARCH_URL = "/rest/api/search";
    String CONFLUENCE_API_CONTENT_URL = "/rest/api/content/";

    Integer CONFLUENCE_PAGE_RATING_MULTIPLIER = 200;


    public User getKeywordUserStats(User toCompleteUser, String keyword){

        ConfluenceSearchResponse response = restTemplate.getForObject(CONFLUENCE_BASE_URL + CONFLUENCE_API_SEARCH_URL
                        + "?cql=creator = " + toCompleteUser.getName() + " and text~" + keyword,
                ConfluenceSearchResponse.class);

        if(response.getResults().size() > 3){
            List<Result> newResults = new ArrayList<>();
            newResults.add(response.getResults().get(0));
            newResults.add(response.getResults().get(1));
            newResults.add(response.getResults().get(2));
            response.setResults(newResults);
        }
        toCompleteUser.setConfluenceArticles(resultsToConfluenceArticleInfo(response.getResults()));

        return toCompleteUser;
    }

    public List<ConfluenceArticleInfo> resultsToConfluenceArticleInfo(List<Result> results){
        return results.stream().map(result -> ConfluenceArticleInfo.builder()
                .summary(result.getContent().getTitle())
                .url("http://hack.kono.lt/display/ds/" + UriUtils.encode(result.getContent().getTitle(), StandardCharsets.UTF_8))
                .build()).collect(Collectors.toList());
    }

    @SneakyThrows
    public ConfluenceSearchResponse getSearchResponse(String keyword){

        return restTemplate.getForObject(CONFLUENCE_BASE_URL + CONFLUENCE_API_SEARCH_URL + "?cql=text~" + keyword, ConfluenceSearchResponse.class);
    }

    @SneakyThrows
    public ConfluenceContentInfoResponse getContentInfoResponse(String contentId){

        URIBuilder uriBuilder = new URIBuilder(CONFLUENCE_BASE_URL + CONFLUENCE_API_CONTENT_URL + contentId);

        return restTemplate.getForObject(uriBuilder.build().toString(), ConfluenceContentInfoResponse.class);
    }

    public List<User> getKeywordTop(String keyword){
        ConfluenceSearchResponse confluenceSearchResponse = getSearchResponse(keyword);

        List<String> contentIds = new ArrayList<>();
        List<User> topUsers = new ArrayList<>();

        for (Result result : confluenceSearchResponse.getResults()){
            contentIds.add(result.getContent().getId());
        }

        List<String> contributors = contentIds.stream()
                .map(this::getContentInfoResponse)
                .map(confluenceContentInfoResponse -> confluenceContentInfoResponse.getVersion().getBy().getUsername())
                .collect(Collectors.toList());

        Map usernameAccurencies = contributors.stream().collect(Collectors.toMap(Function.identity(), v -> 1, Integer::sum));

        usernameAccurencies.forEach((key, value)->{
            topUsers.add(User.builder()
                    .name((String) key)
                    .rating((Integer) value  * CONFLUENCE_PAGE_RATING_MULTIPLIER)
                    .build());
        });

        return topUsers;
    }


}
