package lt.kono.template.services;

import lt.kono.template.domain.JiraIssueInfo;
import lt.kono.template.entities.User;
import lt.kono.template.jira.search.domain.Issue;
import lt.kono.template.jira.search.domain.JiraSearchResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class JiraSearchService {
    @Autowired
    @Qualifier("JiraAccessTemplate")
    RestTemplate restTemplate;

    String JIRA_BASE_URL = "http://dev.kono.lt";
    String JIRA_API_ISSUES_SEARCH_URL = "/rest/api/2/search?jql=status=done AND text~";
    Integer JIRA_ISSUE_RATING_MULTIPLIER = 30;

    public User getKeywordUserStats(User toCompleteUser, String keyword){

        JiraSearchResponse response = restTemplate
                .getForObject(JIRA_BASE_URL + JIRA_API_ISSUES_SEARCH_URL + keyword + " AND assignee=" + toCompleteUser.getName(),
                        JiraSearchResponse.class);

        if(response.getIssues().size() > 3){
            List<Issue> newIssues = new ArrayList<>();
            newIssues.add(response.getIssues().get(0));
            newIssues.add(response.getIssues().get(1));
            newIssues.add(response.getIssues().get(2));
            response.setIssues(newIssues);
        }
        toCompleteUser.setJiraIssues(issuesToJiraIssuesInfo(response.getIssues()));

        return toCompleteUser;
    }

    public List<JiraIssueInfo> issuesToJiraIssuesInfo(List<Issue> issues){
        return issues.stream().map(issue -> JiraIssueInfo.builder()
                .summary(issue.getFields().getSummary())
                .url("http://dev.kono.lt/browse/" + issue.getKey())
                .build()).collect(Collectors.toList());
    }

    public List<User> getKeywordTop(String keyword){

        List<User> topUsers = new ArrayList<>();
        JiraSearchResponse response = restTemplate.getForObject(JIRA_BASE_URL + JIRA_API_ISSUES_SEARCH_URL + keyword, JiraSearchResponse.class);

        List<String> contributors = response.getIssues().stream()
                .map(issue -> issue.getFields())
                .map(fields -> fields.getAssignee().getName())
                .collect(Collectors.toList());

        Map usernameAccurencies = contributors.stream().collect(Collectors.toMap(Function.identity(), v -> 1, Integer::sum));

        usernameAccurencies.forEach((key, value)->{
            topUsers.add(User.builder()
                    .name((String) key)
                    .rating((Integer) value  * JIRA_ISSUE_RATING_MULTIPLIER)
                    .build());
        });

        return topUsers;
    }

}
