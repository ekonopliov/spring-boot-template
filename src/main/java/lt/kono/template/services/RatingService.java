package lt.kono.template.services;

import lombok.SneakyThrows;
import lt.kono.template.entities.User;
import lt.kono.template.rating.domain.*;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RatingService {

    @Autowired
    UserService userService;

    RestTemplate restTemplate = new RestTemplate();

    String RATING_API_BASE_URL = "https://a2f08649eff6.ngrok.io";
    String RATING_API_KEYWORD_TOP_URL = "/api/core/users";
    String RATING_API_REVIEW_URL = "/api/core/setUserReview";
    String RATING_API_LEADERBOARD_URL = "/api/core/getTrendingExperts";
    String RATING_API_SOLVED_URL = "/api/core/getUserCases";

    @SneakyThrows
    public RatingSolvedResponse getUserCases(String username){

        try{
            URIBuilder uriBuilder = new URIBuilder(RATING_API_BASE_URL + RATING_API_SOLVED_URL);
            uriBuilder.addParameter("username", username);

            return restTemplate.getForObject(uriBuilder.build().toString(), RatingSolvedResponse.class);
        } catch (HttpClientErrorException ex){
            return null;
        }
    }

    public List<User> getAllTimeLeaderboard(){
        try{
            RatingLeaderboardResponse response = restTemplate
                    .getForObject(RATING_API_BASE_URL + RATING_API_LEADERBOARD_URL, RatingLeaderboardResponse.class);

            return response.getUsers().stream()
                    .map(leaderboardRow -> User.builder()
                           .name(leaderboardRow.getUsername())
                           .rating(leaderboardRow.getContributionRating())
                           .build())
                    .map(user -> {
                        User retrievedUser = userService.getUser(user.getName());
                        retrievedUser.setRating(user.getRating());
                        return retrievedUser;
                    })
            .collect(Collectors.toList());
        } catch (HttpClientErrorException ex){
            return null;
        }
    }

    @SneakyThrows
    public KeywordTopResponse getKeywordTop(String keyword){

        URIBuilder uriBuilder = new URIBuilder(RATING_API_BASE_URL + RATING_API_KEYWORD_TOP_URL);
        uriBuilder.addParameter("keyword", keyword);
        try{
            return restTemplate.getForObject(uriBuilder.build().toString(), KeywordTopResponse.class);
        } catch (HttpClientErrorException ex){
            return null;
        }
    }

    @SneakyThrows
    public List<User> getKeywordTopUsers(String keyword){

        List<User> ratingUsers = new ArrayList<>();
        KeywordTopResponse response = getKeywordTop(keyword);
        if(response == null){
            return new ArrayList<>();
        }
        for (KeywordTopRow row: response.getUserRatings()){
            ratingUsers.add(User.builder()
                    .name(row.getUsername())
                    .rating(row.getContributionRating())
                    .build());
        }

        return ratingUsers;
    }

    @SneakyThrows
    public void addUserReview(String keyword, String username, Integer reviewScore){

        restTemplate.postForObject(RATING_API_BASE_URL + RATING_API_REVIEW_URL,
                ReviewRequest.builder()
                        .username(username)
                        .keyword(keyword)
                        .reviewScore(reviewScore)
                        .build(),
                String.class);
    }


    @SneakyThrows
    public void addUserReview(ReviewRequest reviewRequest){

        restTemplate.postForObject(RATING_API_BASE_URL + RATING_API_REVIEW_URL,
                reviewRequest,
                String.class);
    }
}
