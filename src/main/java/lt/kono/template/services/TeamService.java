package lt.kono.template.services;

import lt.kono.template.entities.Team;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class TeamService {

    List<Team> teams = new ArrayList<>();

    public String getTeamName(String username){
         Optional<Team> retrievedTeam = teams.stream().filter(team -> team.getTeamMembersUsernames().contains(username)).findAny();
         if(retrievedTeam.isPresent()){
             return retrievedTeam.get().getTeamName();
         } else return null;
    }

    @PostConstruct
    void postConstruct(){
        teams.add(Team.builder()
                .teamName("IT Solutions")
                .teamMembersUsernames(Arrays.asList("hack", "vruble"))
                .build());

        teams.add(Team.builder()
                .teamName("Digital Payments")
                .teamMembersUsernames(Arrays.asList("jaroslav", "marcin", "brandauskas"))
                .build());

        teams.add(Team.builder()
                .teamName("Mobile Team")
                .teamMembersUsernames(Arrays.asList("oskar"))
                .build());
    }
}
