package lt.kono.template.services;

import lombok.SneakyThrows;
import lt.kono.template.entities.Team;
import lt.kono.template.entities.User;
import lt.kono.template.rating.domain.RatingSolvedResponse;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    @Qualifier("JiraAccessTemplate")
    RestTemplate restTemplate;

    @Autowired
    RatingService ratingService;

    @Autowired
    ConfluenceSearchService confluenceSearchService;

    @Autowired
    JiraSearchService jiraSearchService;

    @Autowired
    TeamService teamService;

    String JIRA_BASE_URL = "http://dev.kono.lt";
    String JIRA_API_USER_BASE_URL = "/rest/api/2/user";

    @SneakyThrows
    public User getUser(String username){

        try{
            URIBuilder uriBuilder = new URIBuilder(JIRA_BASE_URL + JIRA_API_USER_BASE_URL);
            uriBuilder.addParameter("username", username);


            User user = restTemplate.getForObject(uriBuilder.build().toString(), User.class);
            RatingSolvedResponse ratingSolvedResponse = ratingService.getUserCases(user.getName());
            if (ratingSolvedResponse != null) {
                user.setReceivedCases(ratingSolvedResponse.getReceivedCases());
                user.setSolvedCases(ratingSolvedResponse.getSolvedCases());
            }

            user.setTeamName(teamService.getTeamName(username));
            user.setAvatar(user.getAvatarUrls().get("48x48"));
            return user;
        } catch (HttpClientErrorException ex){
            return null;
        }
    }

    public List<User> getKeywordTopUsers(String keyword){

        List<User> ratingUsers = ratingService.getKeywordTopUsers(keyword);
        List<User> confluenceUsers = confluenceSearchService.getKeywordTop(keyword);
        List<User> jiraUsers = jiraSearchService.getKeywordTop(keyword);

        List<User> topUsers = new ArrayList<>();
        List<User> topUsersWithInfo = new ArrayList<>();

        topUsers.addAll(ratingUsers);
        topUsers.addAll(confluenceUsers);
        topUsers.addAll(jiraUsers);

        Map topUsersMap = topUsers.stream().collect(
                Collectors.groupingBy(User::getName, Collectors.summingInt(User::getRating)));

        topUsersMap = sortByValue(topUsersMap);

        topUsersMap.forEach((key, value) ->{
            User retrievedUser = getUser((String) key);
            if(retrievedUser != null){
                retrievedUser.setRating((Integer) value);
                topUsersWithInfo.add(retrievedUser);
            }
        });

        Collections.reverse(topUsersWithInfo);

        return topUsersWithInfo;
    }

    private static <K, V> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new LinkedList<>(map.entrySet());
        Collections.sort(list, new Comparator<Object>() {
            @SuppressWarnings("unchecked")
            public int compare(Object o1, Object o2) {
                return ((Comparable<V>) ((Map.Entry<K, V>) (o1)).getValue()).compareTo(((Map.Entry<K, V>) (o2)).getValue());
            }
        });

        Map<K, V> result = new LinkedHashMap<>();
        for (Iterator<Map.Entry<K, V>> it = list.iterator(); it.hasNext();) {
            Map.Entry<K, V> entry = (Map.Entry<K, V>) it.next();
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }

}
